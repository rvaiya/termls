# Description

Dumps information about running terminal emulators. Useful for finding
that terminal that got lost in the window haystack.  Pairs nicely with
xmenu and some window manager glue.

# Impetus

Scraping the information from xprop and xwininfo takes deci-seconds
which makes it unsuitable for UI applications.

## Usage

```
# lsterm

WINDOW_ID WINDOW_PROCESS_ID TERMINAL_EMULATOR SHELL   COMMAND             CWD                 
01800009  6228              urxvt             bash                        /home/rvaiya          
03800009  11236             urxvt             bash                        /home/rvaiya          
02600009  5918              urxvt             bash    man proc            /home/rvaiya          
0100011b  11463             emacs             bash                        /home/rvaiya/projects/lsterm

# lsterm -f WINDOW_ID,COMMAND -d,

WINDOW_ID,COMMAND
01c00009,
02a00009,
02600009,man proc 
02c00009,
``` 

# Dependencies

 - libxcb
 - Linux (proc info is obtained non-portably)
 - An ICCCM compliant window manager