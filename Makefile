VERSION=0.01

all:
	-mkdir bin 2> /dev/null
	gcc -DVERSION=\"$(VERSION)\" -lxcb src/main.c -o bin/lsterm
install:
	install -m 755 bin/lsterm /usr/bin
clean:
	rm -rf bin
