/*
* ---------------------------------------------------------------------
* Copyright (c) 2017      Raheman Vaiya
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
* ---------------------------------------------------------------------
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xcb/xcb.h>
#include <unistd.h>
#include <linux/limits.h>
#include "fields.h"

/* The ones which immediately come to mind... */
const char *shells[] = { "bash", "zsh", "dash", "fish", "dash", "sh", "ksh", "csh" };

struct window {
  uint32_t pid;
  xcb_window_t win;
};

struct term_info {
  char *shell;
  char *command;
  char *cwd;
};

static xcb_atom_t get_atom(xcb_connection_t *con, char *name)
{
  return xcb_intern_atom_reply(con,
                               xcb_intern_atom(con, 1, strlen(name), name),
                               NULL)->atom;
}

struct window *get_windows(xcb_connection_t *con, size_t *sz)
{
  xcb_atom_t WM_STATE_ATOM = get_atom(con, "WM_STATE");
  xcb_atom_t WM_PID_ATOM = get_atom(con, "_NET_WM_PID");

  const int MAX_WINDOWS = 2000;
  xcb_window_t windows[MAX_WINDOWS];
  xcb_query_tree_cookie_t cookies[MAX_WINDOWS];
  xcb_get_property_cookie_t state_cookies[MAX_WINDOWS];
  xcb_get_property_cookie_t pid_cookies[MAX_WINDOWS];
  int w = 0, p = 0;
  int c = 0;
  size_t nents = 0;
  struct window *clients;
  
  clients = malloc(sizeof(clients[0]) * MAX_WINDOWS);
  xcb_screen_iterator_t screen = xcb_setup_roots_iterator(xcb_get_setup(con));
  for (;screen.rem;xcb_screen_next(&screen))
    windows[w++] = screen.data->root;
  
  while(p != w) {
    c = 0;
    while(p < w)
      cookies[c++] = xcb_query_tree(con, windows[p++]);
    
    while(c--) {
      xcb_query_tree_reply_t *repl = xcb_query_tree_reply(con, cookies[c], NULL);
      xcb_window_t *wins = xcb_query_tree_children(repl);
      int len = xcb_query_tree_children_length(repl);
      memcpy(windows + w, wins, sizeof(xcb_window_t) * len);
      w += len;
    }
  }

  for (c = 0; c < w; c++) {

    pid_cookies[c] = xcb_get_property(con,
                                      0,
                                      windows[c],
                                      WM_PID_ATOM,
                                      XCB_ATOM_ANY,
                                      0, ~0);

    state_cookies[c] = xcb_get_property(con,
                                        0,
                                        windows[c],
                                        WM_STATE_ATOM,
                                        XCB_ATOM_ANY,
                                        0, ~0);
  }
  
  while(c--) {
    xcb_get_property_reply_t *repl;
    int ismapped;
    ismapped = xcb_get_property_value_length(xcb_get_property_reply(con, state_cookies[c], NULL));

    if(ismapped) {
      repl = xcb_get_property_reply(con, pid_cookies[c], NULL);
      clients[nents].pid = *(uint32_t*)xcb_get_property_value(repl);
      clients[nents++].win = windows[c];
    }
  }

  *sz = nents;
  return clients;
}

/* TODO: find a portable way to obtain proc information. */
uint32_t *pid_children(uint32_t pid, size_t *sz)
{
  char path[100];
  char *line = NULL;
  size_t n = 0;
  size_t lsz;
  char *c;
  const size_t MAX_CHILDREN = 8;
  uint32_t *results = malloc(sizeof(uint32_t) * MAX_CHILDREN);
  
  sprintf(path, "/proc/%d/task/%d/children", pid, pid);
  FILE *fp = fopen(path, "r");
  if(!fp) {
    *sz = 0;
    return NULL;
  }
  
  if(getline(&line, &lsz, fp) == -1) {
    *sz = 0;
    return NULL;
  }
  
  for(c = strtok(line, " ");c && n < MAX_CHILDREN;c = strtok(NULL, " ")) {
    results[n++] = atoi(c);
  }
  
  *sz = n;
  return results;
}

char *pidcwd(uint32_t pid)
{
  static char path[100];
  char *realpath = malloc(PATH_MAX);
  int n = 0;
  char *line = NULL;
  
  sprintf(path, "/proc/%d/task/%d/cwd", pid, pid);
  n = readlink(path, realpath, PATH_MAX);
  if(n == -1) {
    free(realpath);
    return NULL;
  }
  realpath[n] = '\0';
  return realpath;
}

char *pidcmd(uint32_t pid)
{
  static char path[100];
  size_t sz = 0, n;
  char *line = NULL;
  
  sprintf(path, "/proc/%d/task/%d/cmdline", pid, pid);
  FILE *fp = fopen(path, "r");
  if(!fp) return NULL;
  n = getline(&line, &sz, fp);
  while(n--)
    if(line[n] == '\0') line[n] = ' ';
  return line;
}

char *pidprog(uint32_t pid)
{
  static char path[100];
  size_t sz = 0, n;
  char *line = NULL;
  
  sprintf(path, "/proc/%d/task/%d/comm", pid, pid);
  FILE *fp = fopen(path, "r");
  if(!fp) return NULL;
  n = getline(&line, &sz, fp);
  line[n-1] = '\0';
  return line;
}

void free_term_info(struct term_info *tinfo) {
  free(tinfo->shell);
  free(tinfo->cwd);
  free(tinfo->command);
  free(tinfo);
}

struct term_info *term_info(uint32_t term_pid)
{
  int i;
  size_t ntc;
  struct term_info *info = NULL;
  uint32_t *term_children = pid_children(term_pid, &ntc);
  uint32_t *child;
  char *child_cmd;
  
  for(child = term_children;ntc;ntc--, child++) {
    child_cmd = pidprog(*child);
    for (i = 0; i < sizeof(shells)/sizeof(shells[0]); i++) {
      if(!strcmp(shells[i], child_cmd)) {
        size_t nc;
        uint32_t *children;
      
        info = malloc(sizeof(struct term_info));
        info->shell = child_cmd;
        info->command = NULL;
        info->cwd = pidcwd(*child);
      
        children = pid_children(*child, &nc);
        if(children) info->command = pidcmd(children[0]);
        free(children);
        return info;
      }
    }
    free(child_cmd);
  }
  
  return NULL;
}


enum field *parse_field_spec(const char *_str, size_t *sz) {
  char *str = strdup(_str);
  char *col;
  int n = 0, found;
  int nfields = sizeof(fields)/sizeof(fields[0]);
  enum field *result = malloc(sizeof(enum field) * nfields);
  
  for(col = strtok(str, ",");col;col = strtok(NULL, ",")) {
    int i;
    found = 0;
    for (i = 0; i < nfields; i++) {
      if(!strcmp(fields[i].name, col)) {
        found++;
        result[n++] = i;
      }
    }
    if(!found) {
      *sz = 0;
      free(result);
      return NULL;
    }
  }
  
  *sz = n;
  return result;
}

void print_usage(char *cmd) {
  printf("Usage: %s [ -f <FORMAT> | -d <DELIMETER> | -h ]\n", cmd);
}

void print_help(char *cmd) {
  const char *help_string =
    "Description:\n\
\n\
Prints information about terminal emulators in the current X session.\n\
\n\
Args:\n\
    -h: Prints this help message\n\
    -d <char>: Separates columns with <char> the default behaviour is to use space based fixed width fields.\n\
    -f <COLUMN>[,<COLUMN>...]: Filters the output so it only includes the specified columns. Order is preserved.\n\
\n";

  printf(help_string);
  print_usage(cmd);
}

struct opts {
  size_t nfields;
  enum field *fields;
  char *delim;
};
  
void print_version() {
  printf("Version: "VERSION"\n\nWritten By: Raheman Vaiya\n");
}

struct opts parse_args(int argc, char **argv) {
  char c;
  struct opts opts = {0};
  while((c = getopt(argc, argv, "vhf:d:")) != -1) {
    switch(c) {
      case 'f':
        opts.fields = parse_field_spec(optarg, &opts.nfields);
        if(!opts.fields) {
          fprintf(stderr, "ERROR: %s is not a valid field specification.\n", optarg);
          exit(1);
        }
        break;
      case 'd':
        opts.delim = optarg;
        if(strlen(optarg) != 1) {
          fprintf(stderr, "ERROR: delim must be a single character.\n");
          exit(1);
        }
        break;
      case 'h':
        print_help(argv[0]);
        exit(1);
        break;
      case 'v':
        print_version();
        exit(1);
        break;
      default:
        print_usage(argv[0]);
        exit(1);
    }
  }
  
  if(optind != argc) {
    print_usage(argv[0]);
    exit(1);
  }  
  
  return opts;
}

int main(int argc, char **argv)
{
  xcb_connection_t *con;
  struct window *windows, *win;
  size_t w;
  size_t nw;
  int i;
  struct opts opts;
  enum field *active_fields;
  size_t active_fields_len;
  
  con = xcb_connect(NULL, NULL);
  
  if(xcb_connection_has_error(con)) {
    fprintf(stderr, "Failed to open connection to X server.\n");
    exit(1);
  }
  
  opts = parse_args(argc, argv);
  
  active_fields = all_fields;
  active_fields_len = sizeof(all_fields)/sizeof(all_fields[0]);
      
  
  if(opts.fields) {
    active_fields = opts.fields;
    active_fields_len = opts.nfields;
  }
  
  /* Header */
  for (i = 0; i < active_fields_len; i++)
    if(opts.delim)
      if(i == (active_fields_len - 1))
        printf("%s", fields[active_fields[i]].name);
      else
        printf("%s%c", fields[active_fields[i]].name, *opts.delim);
    else
      printf("%-*s", fields[active_fields[i]].width, fields[active_fields[i]].name);
  printf("\n");
        
  windows = get_windows(con, &nw);
  for (win = windows;nw;nw--, win++) {
    struct term_info *tinfo;
    uint32_t *offspring;
    size_t nc;
          
    tinfo = term_info(win->pid);
    if(tinfo) {
      int i;
      char *term = pidprog(win->pid);
      for (i = 0; i < active_fields_len; i++) {
        int width = fields[active_fields[i]].width;
        switch(active_fields[i]) {
#define case(col, spec, exp)                            \
          case col:                                     \
            if(opts.delim)                              \
              if(i == (active_fields_len - 1))          \
                printf("%"spec, exp);                   \
              else                                      \
                printf("%"spec"%c", exp, *opts.delim);  \
            else                                        \
              printf("%-*"spec, width, exp);            \
            break;                             
                          
          case(WINDOW_ID, "lx", win->win);
          case(WINDOW_PROCESS_ID, "d", win->pid);
          case(TERMINAL_EMULATOR, "s", term);
          case(SHELL, "s", tinfo->shell);
          case(COMMAND, "s", tinfo->command ? tinfo->command : "");
          case(CWD, "s", tinfo->cwd);
#undef print
                
        }
      }
            
      printf("\n");
      free_term_info(tinfo);
    }
  }
  return 0;
}
